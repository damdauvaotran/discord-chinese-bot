const axios = require('axios');
const moment = require('moment');
const Discord = require('discord.js');

const chingChong = require('./src/command/ching-chong/ching-chong')

const client = new Discord.Client();
const token = process.env.ACCESS_TOKEN || 'NjAzNjIzNDg5NDcxMjUwNDMz.XTm71g.LVfsc2e6BZf4c1B8IGTi82HF3GA'
const DEFAULT_RESPONSE = "Ching cc";
const APP_NAME = 'ching-chong-man';
const APP_URL = `https://${APP_NAME}.herokuapp.com/`;

const matchRegex = new RegExp('^\>ching chong (.*)$', 'g', 'u');
const checkingRegex = new RegExp('^\>ching chong .*$', 'g', 'u.');

const botEndpoints = [
    {
        command: '>ching chong',
        handler: chingChong
    }
];


client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    const { content } = msg
    if (content) {
        for (let endPoint of botEndpoints) {
            let breakFlag = false;
            if (content.startsWith(endPoint.command)) {
                breakFlag = true;
                endPoint.handler(msg)
            }
            if (breakFlag) {
                break;
            }
        }
    }
});

client.login(token);



// Keep server awake

const getRandom = (from = 1, to = 29) => {
    return Math.floor((Math.random() * Math.floor(to - from)) + from);
}

const pingDistanceDuration = moment.duration(19, 'm').asMilliseconds()
setInterval(function () {
    axios.get(APP_URL).catch(e => { });
}, pingDistanceDuration);





