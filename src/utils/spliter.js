const spliter = (messageContent, splitRegex) => {
    const splitedMessage = splitRegex.exec(messageContent);
    return splitedMessage
}

module.exports = spliter;