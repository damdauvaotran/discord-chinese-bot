const spliter = require('../../utils/spliter');
const axios = require('axios');

const DEFAULT_RESPONSE = "Ching cc";
const DEFAULT_CANNOT_SPLIT_RESPONSE = "Sai cú fáp à";

const SPLITING_REGEX = new RegExp('^\>ching chong (.*)$', 'u');
const CHECKING_REGEX = new RegExp('^\>ching chong .*$', 'u');


const chingChongBot = async (msg) => {
    const { content } = msg;
    const splitedMessage = spliter(content, SPLITING_REGEX)
    if (splitedMessage && splitedMessage.length > 1) {
        try {
            const targetContent = splitedMessage[1];
            const translatedContent = await translateToChinese(targetContent);
            msg.reply(translatedContent)
        } catch (e) {
            console.error('Error at chingChongBot ', e)
            msg.reply(DEFAULT_CANNOT_SPLIT_RESPONSE)
        }

    } else {
        console.error('Cannot splite', splitedMessage)
        msg.reply(DEFAULT_CANNOT_SPLIT_RESPONSE)
    }

}


const translateToChineseApiCall = async (sourceText) => {
    const sourceLang = 'auto'
    const targetLang = 'zh-CN'
    const url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
        + sourceLang + "&tl=" + targetLang + "&dt=t&q=" + encodeURI(sourceText);
    try {
        const respose = await axios.get(url)
        return respose
    } catch (e) {
        console.error('Error at translateToChineseApiCall ', e)
    }
}

const translateToChinese = async (targetContent) => {
    try {
        const res = await translateToChineseApiCall(targetContent)
        const { data } = res;
        const translatedTargetContent = data && data[0] && data[0][0] && data[0][0][0] || DEFAULT_RESPONSE
        return translatedTargetContent
    } catch (e) {
        console.error('Error at translateToChinese ', e)
        return DEFAULT_RESPONSE
    }
}



module.exports = chingChongBot